<!-- HEADER & SHIELDS -->
<div align="center">
<h1>Personal Dotfiles</h1>

![](https://img.shields.io/gitlab/last-commit/brook-seyoum/dotfiles?color=212121&logo=&logoColor=131821&style=for-the-badge)
![](https://img.shields.io/gitlab/stars/brook-seyoum/dotfiles?color=212121&logo=&logoColor=131821&style=for-the-badge)

</div>


## Description

A Simple, Performant and Minimalistic dotfiles for Me Workflow

## Utilities 

|     App     |    Link     |
| ----------- | ----------- |
| **Terminal**     | [Alacritty][ALACRITTY]|
| **Web browser**  | [Firefox][FIREFOX]    |
| **Editor**       | [Neovim][NEOVIM]      |
| **Shell**        | [Bash][BASH]          |
| **Another Shell**| [Fish][FISH]          |
| **Hotel**        | [Trivago :)][RICKROLL]| <!--RICK ROLL LINK-->

## Other
|     App     |    Link     |
| ----------- | ----------- |
| **lazygit** |  [Lazygit][LAZYGIT]          |


<!-- LINKS -->
[RICKROLL]: https://www.youtube.com/watch?v=dQw4w9WgXcQ
[NEOVIM]: https://github.com/neovim/neovim
[FIREFOX]: https://www.mozilla.org/en-US/firefox/new/
[ALACRITTY]: https://github.com/alacritty/alacritty
[BASH]: https://github.com/gitGNU/gnu_bash
[FISH]: https://github.com/fish-shell/fish-shell
[LAZYGIT]: https://github.com/jesseduffield/lazygit
